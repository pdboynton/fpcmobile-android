package com.firstpentecostalchurch.FPCMobile.view.activity.page;

import com.firstpentecostalchurch.FPCMobile.SharedConstants;
import com.firstpentecostalchurch.FPCMobile.activity.ECal;
import com.firstpentecostalchurch.FPCMobile.activity.News;
import com.firstpentecostalchurch.FPCMobile.activity.PBlog;
import com.firstpentecostalchurch.FPCMobile.activity.R;
import com.firstpentecostalchurch.FPCMobile.activity.SGroups;
import com.firstpentecostalchurch.FPCMobile.activity.SNotes;
import com.firstpentecostalchurch.FPCMobile.activity.WCast;
import com.firstpentecostalchurch.FPCMobile.control.ControlFactory;
import com.firstpentecostalchurch.FPCMobile.control.page.CurrentPageManager;
import com.firstpentecostalchurch.FPCMobile.view.activity.base.ActivityBase;
import com.firstpentecostalchurch.FPCMobile.view.activity.base.Dialogs;
import com.firstpentecostalchurch.FPCMobile.view.activity.bookmark.Bookmarks;
import com.firstpentecostalchurch.FPCMobile.view.activity.comparetranslations.CompareTranslations;
import com.firstpentecostalchurch.FPCMobile.view.activity.download.Download;
import com.firstpentecostalchurch.FPCMobile.view.activity.footnoteandref.FootnoteAndRefActivity;
import com.firstpentecostalchurch.FPCMobile.view.activity.help.Help;
import com.firstpentecostalchurch.FPCMobile.view.activity.mynote.MyNotes;
import com.firstpentecostalchurch.FPCMobile.view.activity.navigation.ChooseDocument;
import com.firstpentecostalchurch.FPCMobile.view.activity.navigation.History;
import com.firstpentecostalchurch.FPCMobile.view.activity.readingplan.DailyReading;
import com.firstpentecostalchurch.FPCMobile.view.activity.readingplan.ReadingPlanSelectorList;
import com.firstpentecostalchurch.FPCMobile.view.activity.settings.SettingsActivity;
import com.firstpentecostalchurch.FPCMobile.view.activity.speak.Speak;
import com.firstpentecostalchurch.FPCMobile.view.util.DataPipe;
import net.bible.service.common.CommonUtils;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/** Handle requests from the main menu
 * 
 * @author denha1m
 *
 */
public class MenuCommandHandler {

	private LongPressControl longPressControl = new LongPressControl();
	
	private static final String TAG = "MainMenuCommandHandler";
	
	public static class IntentHolder {
		Intent intent;
		int requestCode;
	}
	
	private MainBibleActivity callingActivity;
	
	// request codes passed to and returned from sub-activities
	static final int REFRESH_DISPLAY_ON_FINISH = 2;
	static final int UPDATE_SUGGESTED_DOCUMENTS_ON_FINISH = 3;

	private String mPrevLocalePref = "";
	
	public MenuCommandHandler(MainBibleActivity activity) {
		super();
		this.callingActivity = activity;
	}
	
	public boolean isIgnoreLongPress() {
		return longPressControl.isIgnoreLongPress();
	}

	/** 
     * on Click handlers
     */
    public boolean handleMenuRequest(int menuItemId) {
        boolean isHandled = false;
        int requestCode = ActivityBase.STD_REQUEST_CODE;
        
    	// Activities
    	{
    		Intent handlerIntent = null;
	        if (menuItemId == R.id.chooseBookButton) {
				handlerIntent = new Intent(callingActivity, ChooseDocument.class);
			} else if (menuItemId == R.id.selectPassageButton) {
				handlerIntent = new Intent(callingActivity, CurrentPageManager.getInstance().getCurrentPage().getKeyChooserActivity());
			} else if (menuItemId == R.id.searchButton) {
				handlerIntent = ControlFactory.getInstance().getSearchControl().getSearchIntent(CurrentPageManager.getInstance().getCurrentPage().getCurrentDocument());
			} else if (menuItemId == R.id.settingsButton) {
				handlerIntent = new Intent(callingActivity, SettingsActivity.class);
				// force the bible view to be refreshed after returning from settings screen because notes, verses, etc. may be switched on or off
	        	mPrevLocalePref = CommonUtils.getLocalePref();
				requestCode = REFRESH_DISPLAY_ON_FINISH;
			} else if (menuItemId == R.id.historyButton) {
				handlerIntent = new Intent(callingActivity, History.class);
			} else if (menuItemId == R.id.bookmarksButton) {
				handlerIntent = new Intent(callingActivity, Bookmarks.class);
			} else if (menuItemId == R.id.mynotesButton) {
				handlerIntent = new Intent(callingActivity, MyNotes.class);
			} else if (menuItemId == R.id.speakButton) {
				handlerIntent = new Intent(callingActivity, Speak.class);
			} else if (menuItemId == R.id.dailyReadingPlanButton) {
				// show todays plan or allow plan selection
	        	if (ControlFactory.getInstance().getReadingPlanControl().isReadingPlanSelected()) {
	        		handlerIntent = new Intent(callingActivity, DailyReading.class);
	        	} else {
	        		handlerIntent = new Intent(callingActivity, ReadingPlanSelectorList.class);
	        	}
			} else if (menuItemId == R.id.downloadButton) {
				if (CommonUtils.getSDCardMegsFree()<SharedConstants.REQUIRED_MEGS_FOR_DOWNLOADS) {
	            	Dialogs.getInstance().showErrorMsg(R.string.storage_space_warning);
	        	} else if (!CommonUtils.isInternetAvailable()) {
	            	Dialogs.getInstance().showErrorMsg(R.string.no_internet_connection);
	        	} else {
	        		handlerIntent = new Intent(callingActivity, Download.class);
	        		requestCode = UPDATE_SUGGESTED_DOCUMENTS_ON_FINISH;
	        	}
			} else if (menuItemId == R.id.helpButton) {
				handlerIntent = new Intent(callingActivity, Help.class);
			} else if (menuItemId == R.id.backup) {
				ControlFactory.getInstance().getBackupControl().backupDatabase();
				isHandled = true;
			} else if (menuItemId == R.id.restore) {
				ControlFactory.getInstance().getBackupControl().restoreDatabase();
				isHandled = true;
			} else if (menuItemId == R.id.compareTranslations) {
				handlerIntent = new Intent(callingActivity, CompareTranslations.class);
			} else if (menuItemId == R.id.newsfeed) {
				handlerIntent = new Intent(callingActivity, News.class);
			} else if (menuItemId == R.id.pblog) {
				handlerIntent = new Intent(callingActivity, PBlog.class);
			} else if (menuItemId == R.id.eventCal) {
				handlerIntent = new Intent(callingActivity, ECal.class);
			} else if (menuItemId == R.id.sermonNotes) {
				handlerIntent = new Intent(callingActivity, SNotes.class);
			} else if (menuItemId == R.id.webCast) {
				handlerIntent = new Intent(callingActivity, WCast.class);
			} else if (menuItemId == R.id.smallGroups) {
				handlerIntent = new Intent(callingActivity, SGroups.class);
			} else if (menuItemId == R.id.notes) {
				handlerIntent = new Intent(callingActivity, FootnoteAndRefActivity.class);
				// pump the notes into the viewer (there must be an easier way other than Parcelable)
	        	//TODO refactor so the notes are loaded by the Notes viewer using a separate SAX parser 
	        	DataPipe.getInstance().pushNotes(callingActivity.getBibleContentManager().getNotesList());
			} else if (menuItemId == R.id.add_bookmark) {
				ControlFactory.getInstance().getBookmarkControl().bookmarkCurrentVerse();
				// refresh view to show new bookmark icon
				callingActivity.getBibleContentManager().updateText(true);
				isHandled = true;
			} else if (menuItemId == R.id.myNoteAddEdit) {
				CurrentPageManager.getInstance().showMyNote();
				isHandled = true;
			} else if (menuItemId == R.id.copy) {
				ControlFactory.getInstance().getPageControl().copyToClipboard();
				isHandled = true;
			} else if (menuItemId == R.id.shareVerse) {
				ControlFactory.getInstance().getPageControl().shareVerse();
				isHandled = true;
			} else if (menuItemId == R.id.selectText) {
				// ICS+ have their own ui prompts for copy/paste
	        	if (!CommonUtils.isIceCreamSandwichPlus()) {
	        		Toast.makeText(callingActivity, R.string.select_text_help, Toast.LENGTH_LONG).show();
	        	}
				callingActivity.getDocumentViewManager().getDocumentView().selectAndCopyText(longPressControl);
				isHandled = true;
			}
	        
	        if (handlerIntent!=null) {
	        	callingActivity.startActivityForResult(handlerIntent, requestCode);
	        	isHandled = true;
	        } 
    	}

        return isHandled;
    }
    
    public boolean restartIfRequiredOnReturn(int requestCode) {
    	if (requestCode == MenuCommandHandler.REFRESH_DISPLAY_ON_FINISH) {
    		Log.i(TAG, "Refresh on finish");
    		if (!CommonUtils.getLocalePref().equals(mPrevLocalePref)) {
    			// must restart to change locale
    			PendingIntent intent = PendingIntent.getActivity(callingActivity.getBaseContext(), 0, new Intent(callingActivity.getIntent()), callingActivity.getIntent().getFlags());
    			AlarmManager mgr = (AlarmManager)callingActivity.getSystemService(Context.ALARM_SERVICE);
    			mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, intent);
    			System.exit(2);
    			return true;
    		}
    	}
    	return false;
    }

    public boolean isDisplayRefreshRequired(int requestCode) { 
    	return requestCode == MenuCommandHandler.REFRESH_DISPLAY_ON_FINISH;
	}
    
    public boolean isDocumentChanged(int requestCode) { 
    	return requestCode == MenuCommandHandler.UPDATE_SUGGESTED_DOCUMENTS_ON_FINISH;
    }

}
