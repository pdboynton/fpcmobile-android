package com.firstpentecostalchurch.FPCMobile.view.activity.base.toolbar;

import com.firstpentecostalchurch.FPCMobile.activity.R;
import com.firstpentecostalchurch.FPCMobile.control.ControlFactory;
import com.firstpentecostalchurch.FPCMobile.control.event.passage.PassageEvent;
import com.firstpentecostalchurch.FPCMobile.control.event.passage.PassageEventListener;
import com.firstpentecostalchurch.FPCMobile.control.event.passage.PassageEventManager;
import com.firstpentecostalchurch.FPCMobile.control.page.CurrentPageManager;
import com.firstpentecostalchurch.FPCMobile.view.activity.base.CurrentActivityHolder;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class CurrentPageToolbarButton extends ToolbarButtonBase implements ToolbarButton {

	private Button mButton;
	private String mCurrentPageTitle;
	
	@SuppressWarnings("unused")
	private static final String TAG = "Toolbar";
	private ToolbarButtonHelper helper = new ToolbarButtonHelper();
	
	public CurrentPageToolbarButton(View parent) {
        mButton = (Button)parent.findViewById(R.id.titlePassage);

        mButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	onButtonPress();
            }
        });

        // listen for verse change events
        PassageEventManager.getInstance().addPassageEventListener(new PassageEventListener() {
			@Override
			public void pageDetailChange(PassageEvent event) {
				update();
			}
		});
	}

	private void onButtonPress() {
		// load Document selector
		Activity currentActivity = CurrentActivityHolder.getInstance().getCurrentActivity();
    	Intent pageHandlerIntent = new Intent(currentActivity, CurrentPageManager.getInstance().getCurrentPage().getKeyChooserActivity());
    	currentActivity.startActivityForResult(pageHandlerIntent, 1);
	}

	public void update() {
        mCurrentPageTitle = ControlFactory.getInstance().getPageControl().getCurrentPageTitle();

        // must do ui update in ui thread
        // copy title to ensure it isn't changed before ui thread executes the following
        final String title = mCurrentPageTitle;
		mButton.post(new Runnable() {
			@Override
			public void run() {
		        helper.updateButtonText(title, mButton);
			}
		});
	}

	@Override
	public boolean canShow() {
		return mCurrentPageTitle!=null;
	}

	@Override
	public int getPriority() {
		return 1;
	}
}
