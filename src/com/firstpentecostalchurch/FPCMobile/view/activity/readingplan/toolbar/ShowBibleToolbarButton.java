package com.firstpentecostalchurch.FPCMobile.view.activity.readingplan.toolbar;

import com.firstpentecostalchurch.FPCMobile.activity.R;
import com.firstpentecostalchurch.FPCMobile.control.ControlFactory;
import com.firstpentecostalchurch.FPCMobile.view.activity.base.toolbar.ToolbarButton;

import org.crosswire.jsword.book.Book;

import android.view.View;

public class ShowBibleToolbarButton extends ShowDocumentToolbarButton implements ToolbarButton {

	public ShowBibleToolbarButton(View parent) {
        super(parent, R.id.quickBibleChange);
	}

	@Override
	public Book getDocument() {
		return ControlFactory.getInstance().getCurrentPageControl().getCurrentBible().getCurrentDocument();
	}
}
