package com.firstpentecostalchurch.FPCMobile.view.activity.readingplan.toolbar;

import android.view.View;
import com.firstpentecostalchurch.FPCMobile.view.activity.base.toolbar.speak.SpeakToolbarButton;

public class PauseToolbarButton extends SpeakToolbarButton {

	public PauseToolbarButton(View parent) {
		super(parent);
	}

	// do not show if nothing is being said.  If speaking then allow pause and vice-versa
	@Override
	public boolean canShow() {
		return super.canShow() &&
				(getSpeakControl().isSpeaking() || getSpeakControl().isPaused());
	}
}
