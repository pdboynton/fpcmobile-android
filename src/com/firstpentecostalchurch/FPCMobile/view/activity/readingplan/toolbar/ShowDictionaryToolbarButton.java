package com.firstpentecostalchurch.FPCMobile.view.activity.readingplan.toolbar;

import com.firstpentecostalchurch.FPCMobile.activity.R;
import com.firstpentecostalchurch.FPCMobile.control.ControlFactory;
import com.firstpentecostalchurch.FPCMobile.view.activity.base.toolbar.ToolbarButton;

import org.crosswire.jsword.book.Book;

import android.view.View;

public class ShowDictionaryToolbarButton extends ShowDocumentToolbarButton implements ToolbarButton {

	public ShowDictionaryToolbarButton(View parent) {
        super(parent, R.id.quickDictionaryChange);
	}

	@Override
	public Book getDocument() {
		return ControlFactory.getInstance().getCurrentPageControl().getCurrentDictionary().getCurrentDocument();
	}

	@Override
	public boolean canShow() {
		return super.canShow() && !isNarrow();
	}
	
	
}
