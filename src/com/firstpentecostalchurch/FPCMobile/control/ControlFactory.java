package com.firstpentecostalchurch.FPCMobile.control;

import com.firstpentecostalchurch.FPCMobile.control.backup.BackupControl;
import com.firstpentecostalchurch.FPCMobile.control.bookmark.Bookmark;
import com.firstpentecostalchurch.FPCMobile.control.bookmark.BookmarkControl;
import com.firstpentecostalchurch.FPCMobile.control.comparetranslations.CompareTranslationsControl;
import com.firstpentecostalchurch.FPCMobile.control.document.DocumentControl;
import com.firstpentecostalchurch.FPCMobile.control.download.DownloadControl;
import com.firstpentecostalchurch.FPCMobile.control.footnoteandref.FootnoteAndRefControl;
import com.firstpentecostalchurch.FPCMobile.control.link.LinkControl;
import com.firstpentecostalchurch.FPCMobile.control.mynote.MyNote;
import com.firstpentecostalchurch.FPCMobile.control.mynote.MyNoteControl;
import com.firstpentecostalchurch.FPCMobile.control.page.CurrentPageManager;
import com.firstpentecostalchurch.FPCMobile.control.page.PageControl;
import com.firstpentecostalchurch.FPCMobile.control.page.PageTiltScrollControl;
import com.firstpentecostalchurch.FPCMobile.control.readingplan.ReadingPlanControl;
import com.firstpentecostalchurch.FPCMobile.control.search.SearchControl;
import com.firstpentecostalchurch.FPCMobile.control.speak.SpeakControl;

//TODO replace with ioc (maybe)
/** allow access to control layer
 *
 * @author denha1m
 *
 */
public class ControlFactory {
	//TODO move instance creation here
	private CurrentPageManager currentPageManager = CurrentPageManager.getInstance();
	private DocumentControl documentControl = new DocumentControl();
	private PageControl pageControl = new PageControl();
	private PageTiltScrollControl pageTiltScrollControl = new PageTiltScrollControl();
	private LinkControl linkControl = new LinkControl();
	private SearchControl searchControl = new SearchControl();
	private Bookmark bookmarkControl = new BookmarkControl();
	private MyNote mynoteControl = new MyNoteControl();
	private DownloadControl downloadControl = new DownloadControl();
	private SpeakControl speakControl = new SpeakControl();
	private ReadingPlanControl readingPlanControl = new ReadingPlanControl();
	private CompareTranslationsControl compareTranslationsControl = new CompareTranslationsControl();
	private FootnoteAndRefControl footnoteAndRefControl = new FootnoteAndRefControl();
	private BackupControl backupControl = new BackupControl();
	
	private static ControlFactory singleton = new ControlFactory();
	
	public static ControlFactory getInstance() {
		return singleton;
	}
	
	private ControlFactory() {
		// inject dependencies
		pageControl.setCurrentPageManager(this.currentPageManager);
		readingPlanControl.setSpeakControl(this.speakControl);
		compareTranslationsControl.setCurrentPageManager(currentPageManager);
		footnoteAndRefControl.setCurrentPageManager(currentPageManager);
	}
	
	public DocumentControl getDocumentControl() {
		return documentControl;		
	}

	public PageControl getPageControl() {
		return pageControl;		
	}

	public PageTiltScrollControl getPageTiltScrollControl() {
		return pageTiltScrollControl;
	}

	public SearchControl getSearchControl() {
		return searchControl;		
	}

	public CurrentPageManager getCurrentPageControl() {
		return currentPageManager;		
	}

	public LinkControl getLinkControl() {
		return linkControl;
	}

	/**
	 * @return the bookmarkControl
	 */
	public Bookmark getBookmarkControl() {
		return bookmarkControl;
	}
	
	public MyNote getMyNoteControl() {
		return mynoteControl;
	}

	public DownloadControl getDownloadControl() {
		return downloadControl;
	}

	public SpeakControl getSpeakControl() {
		return speakControl;
	}

	public ReadingPlanControl getReadingPlanControl() {
		return readingPlanControl;
	}

	public CompareTranslationsControl getCompareTranslationsControl() {
		return compareTranslationsControl;
	}

	public FootnoteAndRefControl getFootnoteAndRefControl() {
		return footnoteAndRefControl;
	}

	public BackupControl getBackupControl() {
		return backupControl;
	}
}
