package com.firstpentecostalchurch.FPCMobile.activity;

import android.os.Bundle;
import android.app.Activity;
import android.view.KeyEvent;
//import android.view.Menu;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PBlog extends Activity {
	private WebView pblogWebView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pblog);
		
			pblogWebView = (WebView) findViewById(R.id.pBlog);
			WebSettings webSettings = pblogWebView.getSettings();
			webSettings.setJavaScriptEnabled(true);   
			pblogWebView.setWebViewClient(new WebViewClient());
			pblogWebView.loadUrl("http://m.simplepie.org/?feed=http%3A%2F%2Fwww.firstpentecostalchurch.com%2Fpastor-s-blog%2Fposts.xml");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    // Check if the key event was the Back button and if there's history
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && pblogWebView.canGoBack()) {
	        pblogWebView.goBack();
	        return true;
	    }
	    // If it wasn't the Back key or there's no web page history, bubble up to the default
	    // system behavior (probably exit the activity)
	    return super.onKeyDown(keyCode, event);
	}	
	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}
	
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        boolean isHandled = mainMenuCommandHandler.handleMenuRequest(item.getItemId());
//        
//     	if (!isHandled) {
//            isHandled = super.onOptionsItemSelected(item);
//        }
//        
//    	return isHandled;
//    }
	
}

