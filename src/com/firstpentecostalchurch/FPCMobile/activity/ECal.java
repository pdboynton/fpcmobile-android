package com.firstpentecostalchurch.FPCMobile.activity;

//import com.firstpentecostalchurch.FPCMobile.view.activity.page.MenuCommandHandler;
import android.os.Bundle;
import android.app.Activity;
import android.view.KeyEvent;
//import android.view.Menu;
//import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ECal extends Activity {
	private WebView ecalWebView;
//	private MenuCommandHandler mainMenuCommandHandler;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ecal);
		
			ecalWebView = (WebView) findViewById(R.id.ecal);
			WebSettings webSettings = ecalWebView.getSettings();
			webSettings.setJavaScriptEnabled(true);   
			ecalWebView.setWebViewClient(new WebViewClient());
			ecalWebView.loadUrl("https://www.google.com/calendar/embed?src=firstpentecostalchurch.com_e2hia2v3bc766ff9397qu3eqa8@group.calendar.google.com&ctz=America/New_York&showTitle=0&showPrint=0&showTabs=0&showCalendars=0&mode=AGENDA&wkst=1&bgcolor=%23ffffff");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    // Check if the key event was the Back button and if there's history
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && ecalWebView.canGoBack()) {
	        ecalWebView.goBack();
	        return true;
	    }
	    // If it wasn't the Back key or there's no web page history, bubble up to the default
	    // system behavior (probably exit the activity)
	    return super.onKeyDown(keyCode, event);
	}	

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}
	
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        boolean isHandled = mainMenuCommandHandler.handleMenuRequest(item.getItemId());
//        
//     	if (!isHandled) {
//            isHandled = super.onOptionsItemSelected(item);
//        }
//        
//    	return isHandled;
//    }
	
}


