package com.firstpentecostalchurch.FPCMobile.activity;

import net.bible.service.common.CommonUtils;

import com.firstpentecostalchurch.FPCMobile.SharedConstants;
import com.firstpentecostalchurch.FPCMobile.control.ControlFactory;
import com.firstpentecostalchurch.FPCMobile.control.page.CurrentPageManager;
import com.firstpentecostalchurch.FPCMobile.view.activity.base.Dialogs;
import com.firstpentecostalchurch.FPCMobile.view.activity.bookmark.Bookmarks;
import com.firstpentecostalchurch.FPCMobile.view.activity.comparetranslations.CompareTranslations;
import com.firstpentecostalchurch.FPCMobile.view.activity.download.Download;
import com.firstpentecostalchurch.FPCMobile.view.activity.footnoteandref.FootnoteAndRefActivity;
import com.firstpentecostalchurch.FPCMobile.view.activity.help.Help;
import com.firstpentecostalchurch.FPCMobile.view.activity.mynote.MyNotes;
import com.firstpentecostalchurch.FPCMobile.view.activity.navigation.ChooseDocument;
import com.firstpentecostalchurch.FPCMobile.view.activity.navigation.History;
import com.firstpentecostalchurch.FPCMobile.view.activity.readingplan.DailyReading;
import com.firstpentecostalchurch.FPCMobile.view.activity.readingplan.ReadingPlanSelectorList;
import com.firstpentecostalchurch.FPCMobile.view.activity.settings.SettingsActivity;
import com.firstpentecostalchurch.FPCMobile.view.activity.speak.Speak;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

public class News extends Activity {
	private WebView newsWebView;
	
	final Activity activity = this;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);
		
			newsWebView = (WebView) findViewById(R.id.news);
			WebSettings webSettings = newsWebView.getSettings();
			webSettings.setJavaScriptEnabled(true);   
			newsWebView.setWebViewClient(new WebViewClient()

			);
			newsWebView.loadUrl("http://m.simplepie.org/?feed=http%3A%2F%2Fwww.firstpentecostalchurch.com%2Fwhat-s-happening%2Fposts.xml");
		   //String customHtml = "<html><body><script language="JavaScript" src="http://feed2js.org//feed2js.php?src=http%3A%2F%2Fwww.firstpentecostalchurch.com%2Fwhat-s-happening%2Fposts.xml&targ=popup&utf=y&html=a"  charset="UTF-8" type="text/javascript"></script>
//</body></html>";
//		   newsWebView.loadData(customHtml, "text/html", "UTF-8");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    // Check if the key event was the Back button and if there's history
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && newsWebView.canGoBack()) {
	        newsWebView.goBack();
	        return true;
	    }
	    // If it wasn't the Back key or there's no web page history, bubble up to the default
	    // system behavior (probably exit the activity)
	    return super.onKeyDown(keyCode, event);
	}	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      	int itemId = item.getItemId();
    	if (itemId == R.id.chooseBookButton) {
    		// try com.firstpentecostalchurch.FPCMobile.view.activity.page.MainBibleActivity
    		startActivity(new Intent(this, ChooseDocument.class));
		} else if (itemId == R.id.selectPassageButton) {
			CurrentPageManager.getInstance().getCurrentPage().getKeyChooserActivity();
		} else if (itemId == R.id.searchButton) {
			ControlFactory.getInstance().getSearchControl().getSearchIntent(CurrentPageManager.getInstance().getCurrentPage().getCurrentDocument());
		} else if (itemId == R.id.settingsButton) {
    		startActivity(new Intent(this, SettingsActivity.class));
		} else if (itemId == R.id.historyButton) {
    		startActivity(new Intent(this, History.class));
		} else if (itemId == R.id.bookmarksButton) {
    		startActivity(new Intent(this, Bookmarks.class));
		} else if (itemId == R.id.mynotesButton) {
    		startActivity(new Intent(this, MyNotes.class));
		} else if (itemId == R.id.speakButton) {
    		startActivity(new Intent(this, Speak.class));
		} else if (itemId == R.id.dailyReadingPlanButton) {
			// show todays plan or allow plan selection
        	if (ControlFactory.getInstance().getReadingPlanControl().isReadingPlanSelected()) {
        		startActivity(new Intent(this, DailyReading.class));
        	} else {
        		startActivity(new Intent(this, ReadingPlanSelectorList.class));
        	}
		} else if (itemId == R.id.downloadButton) {
			if (CommonUtils.getSDCardMegsFree()<SharedConstants.REQUIRED_MEGS_FOR_DOWNLOADS) {
            	Dialogs.getInstance().showErrorMsg(R.string.storage_space_warning);
        	} else if (!CommonUtils.isInternetAvailable()) {
            	Dialogs.getInstance().showErrorMsg(R.string.no_internet_connection);
        	} else {
        		startActivity(new Intent(this, Download.class));
        	}
		} else if (itemId == R.id.helpButton) {
    		startActivity(new Intent(this, Help.class));
		} else if (itemId == R.id.backup) {
			ControlFactory.getInstance().getBackupControl().backupDatabase();
			return true;
		} else if (itemId == R.id.restore) {
			ControlFactory.getInstance().getBackupControl().restoreDatabase();
			return true;
		} else if (itemId == R.id.compareTranslations) {
    		startActivity(new Intent(this, CompareTranslations.class));
		} else if (itemId == R.id.newsfeed) {
    		startActivity(new Intent(this, News.class));
		} else if (itemId == R.id.pblog) {
    		startActivity(new Intent(this, PBlog.class));
		} else if (itemId == R.id.eventCal) {
    		startActivity(new Intent(this, ECal.class));
		} else if (itemId == R.id.sermonNotes) {
    		startActivity(new Intent(this, SNotes.class));
		} else if (itemId == R.id.webCast) {
    		startActivity(new Intent(this, WCast.class));
		} else if (itemId == R.id.smallGroups) {
    		startActivity(new Intent(this, SGroups.class));
		} else if (itemId == R.id.notes) {
    		startActivity(new Intent(this, FootnoteAndRefActivity.class));
		} else if (itemId == R.id.add_bookmark) {
			ControlFactory.getInstance().getBookmarkControl().bookmarkCurrentVerse();
			return true;
		} else if (itemId == R.id.myNoteAddEdit) {
			CurrentPageManager.getInstance().showMyNote();
			return true;
		} else if (itemId == R.id.copy) {
			ControlFactory.getInstance().getPageControl().copyToClipboard();
			return true;
		} else if (itemId == R.id.shareVerse) {
			ControlFactory.getInstance().getPageControl().shareVerse();
			return true;
		} else if (itemId == R.id.selectText) {
			// ICS+ have their own ui prompts for copy/paste
        	if (!CommonUtils.isIceCreamSandwichPlus()) {
        		Toast.makeText(this, R.string.select_text_help, Toast.LENGTH_LONG).show();
        	}
			return true;
		}
		return false;

    }
    
}
