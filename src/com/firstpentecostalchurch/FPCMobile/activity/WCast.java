package com.firstpentecostalchurch.FPCMobile.activity;

import android.os.Bundle;
import android.app.Activity;
import android.view.KeyEvent;
//import android.view.Menu;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WCast extends Activity {

	private WebView wcastWebView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_wcast);
			
				wcastWebView = (WebView) findViewById(R.id.wcast);
				WebSettings webSettings = wcastWebView.getSettings();
				webSettings.setJavaScriptEnabled(true);   
				wcastWebView.setWebViewClient(new WebViewClient());
				wcastWebView.loadUrl("http://www.ustream.tv/embed/10786920");
		}

		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
		    // Check if the key event was the Back button and if there's history
		    if ((keyCode == KeyEvent.KEYCODE_BACK) && wcastWebView.canGoBack()) {
		        wcastWebView.goBack();
		        return true;
		    }
		    // If it wasn't the Back key or there's no web page history, bubble up to the default
		    // system behavior (probably exit the activity)
		    
		    return super.onKeyDown(keyCode, event);
		}	
		
//		@Override
//		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
//			getMenuInflater().inflate(R.menu.main, menu);
//			return true;
//		}
		
//	    @Override
//	    public boolean onOptionsItemSelected(MenuItem item) {
//	        boolean isHandled = mainMenuCommandHandler.handleMenuRequest(item.getItemId());
//	        
//	     	if (!isHandled) {
//	            isHandled = super.onOptionsItemSelected(item);
//	        }
//	        
//	    	return isHandled;
//	    }
		
	}

