package com.firstpentecostalchurch.FPCMobile.activity;

import android.os.Bundle;
import android.app.Activity;
import android.view.KeyEvent;
//import android.view.Menu;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class SGroups extends Activity {
	private WebView sgroupsWebView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sgroups);
		
			sgroupsWebView = (WebView) findViewById(R.id.sgroups);
			WebSettings webSettings = sgroupsWebView.getSettings();
			webSettings.setJavaScriptEnabled(true);
			sgroupsWebView.getSettings().setUserAgentString("Safari");
			sgroupsWebView.setWebViewClient(new WebViewClient()
//			{
//			 @Override
//			public void onPageFinished(WebView view, String url)
//			{
//			    sgroupsWebView.loadUrl("javascript:(function() { " +
//			            "document.getElementsByTagName('header')[0].style.display=false " +
//			            "})()");
//			}
//			}
			);
			sgroupsWebView.loadUrl("https://groups.google.com/forum/m/?place=forum/first-pentecostal-church#!forum/first-pentecostal-church");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    // Check if the key event was the Back button and if there's history
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && sgroupsWebView.canGoBack()) {
	        sgroupsWebView.goBack();
	        return true;
	    }
	    // If it wasn't the Back key or there's no web page history, bubble up to the default
	    // system behavior (probably exit the activity)
	    return super.onKeyDown(keyCode, event);
	}	
	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}
	
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        boolean isHandled = mainMenuCommandHandler.handleMenuRequest(item.getItemId());
//        
//     	if (!isHandled) {
//            isHandled = super.onOptionsItemSelected(item);
//        }
//        
//    	return isHandled;
//    }
	
}

